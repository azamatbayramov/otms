import { Link } from 'react-router-dom';

const LinkButtonToHome = ({onClick, text, buttonClass}) => (

  <Link to="/" role="button" onClick={onClick} className={buttonClass}>
    {text}
  </Link>
);

export default LinkButtonToHome;