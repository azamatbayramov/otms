import './App.css'
import TextPage from "./pages/TextPage.jsx";
import LinkPage from "./pages/LinkPage.jsx";
import ShowTextPage from "./pages/ShowTextPage.jsx";
import NotFoundPage from "./pages/NotFoundPage.jsx";
import {Route, Routes} from 'react-router-dom';

function App() {

  return (
    <>
        <Routes>
            <Route path="/" element={<TextPage/>} />
            <Route path="link/:id" element={<LinkPage/>} />
            <Route path="show/:id" element={<ShowTextPage/>} />
            <Route path="*" element={<NotFoundPage/>} />
        </Routes>
    </>
  )
}

export default App
