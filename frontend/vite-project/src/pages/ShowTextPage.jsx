import { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import LinkButtonToHome from "../components/LinkButton.jsx";

const ShowTextPage = () => {

    const [text, setText] = useState('');

    const { id } = useParams();

    const navigate = useNavigate();

    useEffect(() => {
        function getTextFromServer(id) {
            axios.get('http://otms.duckdns.org/api/message/' + id)
            .then(function (response) {
                setText(response.data.text);
            })
            .catch(function (error) {
                console.log(error);
                navigate('/notFound', { replace: true });
            });
        }

        getTextFromServer(id);
    }, []);

    return (
        <>
            <h1>Message:</h1>

            <p className="description">You can access your message only once. </p>

            <div className="text-container-adaptive">
                <p style={{whiteSpace: 'pre-line'}}>{text}</p>
            </div>

            <LinkButtonToHome text="To&nbsp;Home&nbsp;page" buttonClass="button"/>

        </>
    );
};

export default ShowTextPage;
