import LinkButtonToHome from "../components/LinkButton.jsx";

const NotFoundPage = () => {
    return (
        <>
            <h1 className="error-num">404 </h1>
            <h2>Page not found</h2>

            <LinkButtonToHome text="To&nbsp;Home&nbsp;page" buttonClass="button"/>
        </>
    );
};

export default NotFoundPage;