import { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const TextPage = () => {
    const [text, setText] = useState('');
    const [isEmptyWarning, setIsEmptyWarning] = useState(false);
    const navigate = useNavigate();

    const handleSubmit = async () => {
        if (!text) {
            setIsEmptyWarning(true); // Устанавливаем флаг предупреждения, если текст пуст
            return;
        } else {
            setIsEmptyWarning(false); // Если текст не пуст, снимаем предупреждение
        }

        try {
            const response = await axios.post('http://otms.duckdns.org/api/message', {
                text: text,
            });
            const secretId = response.data.secret_id;
            navigate('/link/' + secretId, { replace: true });
        } catch (error) {
            if (error.response && error.response.status === 422) {
                console.log('Error: invalid data');
            } else {
                console.log('Error occurred:', error);
            }
        }
    }

    return (
        <>
            <h1>One Time Message Service</h1>

            <p className="description">You can write your message and send link to it to someone.
                The message will be deleted after it is read.</p>

            {isEmptyWarning && <p className="error-text">You can not send empty message</p>}

            <div className="input-container">

                <textarea
                    spellCheck="false"
                    placeholder="Type here ..."
                    value={text}
                    onChange={(e) => setText(e.target.value)}
                />
            </div>

            <button onClick={handleSubmit} className="button">
                Submit
            </button>
        </>
    );
};

export default TextPage;