import React from 'react';
import {useParams} from "react-router-dom";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import LinkButtonToHome from "../components/LinkButton.jsx";

const LinkPage = () => {

    const {id} = useParams()

    const link = "http://otms.duckdns.org/show/" + id

    const [buttonText, setButtonText] = React.useState('Copy')

    async function handleClick(){
        setButtonText('Copied !')
    }

    return (
        <>
            <h1>Link to message: </h1>

            <div className="description">
                <p>Copy the link below and send it to your recipient.
                    Message will be deleted after one view.</p>

            </div>

            <div className="link-container">
                <p className="link-text">{link}</p>
            </div>

            <CopyToClipboard text={link}>
                <button onClick={handleClick} className="button">
                    {buttonText}
                </button>
            </CopyToClipboard>

            <LinkButtonToHome text="To&nbsp;Home&nbsp;page" buttonClass="button"/>

        </>
    );
};

export default LinkPage;