# One-Time Messages Service

> This is a service that allows users to create one-time messages with unique link that can be shared with other user.  
> The link allows access to the message only once.

## Technologies Used
* Python
* FastAPI
* MongoDB
* Nginx
* React
* Docker
* GitLab CI/CD
* Unit tests
* Grafana
* Prometheus
* Cron
* Bash

## Features
* API is implemented using Python and FastAPI with MongoDB
* User interface is implemented using React
* Nginx is used as a reverse proxy for API requests and frontend server
* Backend application is containerized using Docker
* Unit tests are used to ensure backend correctness and reliability
* Cron and Bash are used to back up the database
* Grafana and Prometheus are used for system monitoring
* Automated testing and deployment pipelines are implemented using GitLab CI/CD

## Screenshots
<figure>
    <img src="/assets/frontend.png" alt="Frontend part" width="1000">
    <figcaption>Message send form of the frontend part of the service.</figcaption>
</figure>
<figure>
    <img src="/assets/backend.png" alt="Backend part" width="1000">
    <figcaption>Documentation page of the backend part of the service.</figcaption>
</figure>

## Setup
### Clone the repository
* `git clone https://gitlab.com/azamatbayramov/otms.git`
* `cd otms`
### Run the backend
* `docker-compose up --build`
### Run the frontend
* `cd frontend/vite-project`
* `npm install`
* `npm run build`

## Usage 
Docker runs the backend with Nginx. API is avaliable at `/api`.

The backend part creates two endpoints:
* `/message` for creating a new message
* `/message/{secret_id}` for reading a message

The Swagger documentation for backend is available at `/docs`.

## Authors
* Darya Koncheva, [d.koncheva@innopolis.university](mailto:d.koncheva@innopolis.university)
* Azamat Bayramov, [a.bayramov@innopolis.university](mailto:a.bayramov@innopolis.university)
* Vladislav Bolshakov, [v.bolshakov@innopolis.university](mailto:v.bolshakov@innopolis.university)
* Dmitriy Okoneshnikov, [d.okoneshnikov@innopolis.university](mailto:d.okoneshnikov@innopolis.university)
