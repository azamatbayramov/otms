from uuid import UUID
from backend.src.models import Message


class Query:
    @staticmethod
    async def create_message(secret_id: UUID, text: str, is_read: bool = False) -> None:
        """
        Insert a new message into the database.

        Args:
            secret_id: the secret id of the message.
            text: the text part of the message.
            is_read: whether the message is read or not.
        """
        new_message = Message(secret_id=secret_id, text=text)
        await Message.insert(new_message)

    @staticmethod
    async def get_message(secret_id: UUID) -> Message | None:
        """
        Get a message from the database or None if it does not exist.

        Args:
            secret_id: the secret id of the message.

        Returns:
            Message or None
        """
        pipeline = {
            "secret_id": secret_id,
        }
        return await Message.find_one(pipeline)

    @staticmethod
    async def read_message(secret_id: UUID) -> None:
        """
        Mark a message read by its secret id.

        Args:
            secret_id: the secret id of the message
        """
        pipeline = {
            "secret_id": secret_id,
        }
        message = await Message.find_one(pipeline)
        message.is_read = True
        await Message.save(message)

    @staticmethod
    async def delete_message(secret_id: UUID) -> None:
        """
        deletes message by its secret id.

        Args:
            secret_id: the secret id of message
        """
        await Message.find_one(Message.secret_id == secret_id).delete_one()
