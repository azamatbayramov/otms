from uuid import UUID

from pydantic import BaseModel


class CreateMessageRequestSchema(BaseModel):
    """
    Schema for accepting a request for creating a message.

    - text: message text.
    """
    text: str


class CreateMessageResponseSchema(BaseModel):
    """
    Schema for returning a response from creating a message.

    - secret_id: unique id of the message.
    """
    secret_id: UUID


class GetMessageResponseSchema(BaseModel):
    """
    Schema for returning a response from getting a message.

    - text: message text.
    """
    text: str


class MessageNonExistingExceptionSchema(BaseModel):
    """
    Schema for exception for non-existent message.
    """
    message: str
