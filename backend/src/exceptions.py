class NonExistingMessageException(Exception):
    """
    Exception raised when a non-existent or already read message is sent.
    """
    pass
