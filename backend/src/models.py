from datetime import datetime
from uuid import UUID

from beanie import Document
from pydantic import Field


class Message(Document):
    secret_id: UUID
    text: str
    created_at: datetime = Field(default_factory=datetime.utcnow)
