from uuid import uuid4, UUID

from fastapi import APIRouter, status

from backend.src.exceptions import NonExistingMessageException
from backend.src.models import Message
from backend.src.repository import Query
from backend.src.schemas import CreateMessageRequestSchema, CreateMessageResponseSchema, GetMessageResponseSchema, \
    MessageNonExistingExceptionSchema

router = APIRouter(
    prefix="/message",
    tags=["Messages"]
)


@router.post(
    "",
    response_model=CreateMessageResponseSchema,
    status_code=status.HTTP_200_OK,
    description="Create a new one time message",
)
async def create_message(request: CreateMessageRequestSchema):
    secret_id = uuid4()
    await Query.create_message(secret_id=secret_id, text=request.text)
    return CreateMessageResponseSchema(secret_id=secret_id)


@router.get(
    "/{secret_id}",
    response_model=GetMessageResponseSchema,
    status_code=status.HTTP_200_OK,
    description="Get one time message",
    responses={
        status.HTTP_404_NOT_FOUND: {
            "model": MessageNonExistingExceptionSchema,
            "description": "Message not found"
        }
    }
)
async def get_message(secret_id: UUID):
    message: Message | None = await Query.get_message(secret_id)
    if message is None:
        raise NonExistingMessageException
    await Query.delete_message(secret_id)
    return GetMessageResponseSchema(text=message.text)
