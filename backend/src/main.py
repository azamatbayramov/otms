from contextlib import asynccontextmanager

from beanie import init_beanie
from fastapi import FastAPI
from fastapi import Request
from fastapi.responses import JSONResponse
from motor.motor_asyncio import AsyncIOMotorClient
from prometheus_fastapi_instrumentator import Instrumentator

from backend.src.config import config
from backend.src.exceptions import NonExistingMessageException
from backend.src.models import Message
from backend.src.routes import router


async def initialize_database():
    """
    Initialize the database.
    """
    client = AsyncIOMotorClient(config.MONGODB_URL)
    await init_beanie(database=client.OTMS, document_models=[Message])


app = FastAPI(
    root_path=config.APP_ROOT_PATH,
    title=config.APP_TITLE,
    description=config.APP_DESCRIPTION,
    version=config.APP_VERSION,
)
app.include_router(router)

instrumentator = Instrumentator().instrument(app)


@app.on_event("startup")
async def _startup():
    await initialize_database()
    instrumentator.expose(app)


@app.exception_handler(NonExistingMessageException)
async def non_existing_message_exception_handler(request: Request, exc: NonExistingMessageException):
    """
    Exception handler for non-existing or already read message exception.

    Args:
        request (Request): The request object.
        exc (NonExistingMessageException): The exception object.

    Returns:
        JSONResponse: A JSON response with the error message.
    """
    return JSONResponse(
        status_code=404,
        content={"message": f"Trying to access non existing or already read message!"},
    )
