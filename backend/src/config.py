class Config:
    MONGODB_URL = "mongodb://mongodb:27017/"

    APP_ROOT_PATH = "/api"
    APP_TITLE = "One-Time Messages Service"
    APP_DESCRIPTION = ("This is a service that allows users to create one-time messages "
                       "with unique link that can be shared with other user. "
                       "The link allows access to the message only once.")
    APP_VERSION = "1.0.0"


config = Config()
