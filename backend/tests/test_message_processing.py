import unittest
from uuid import uuid4

import requests

from backend.tests.config import REQUEST_URL


class MessageCreatingReceivingTestCase(unittest.TestCase):
    text = "Message Creating Receiving"

    def test_usual_message_work(self):
        """
        create message -> read created message.
        """
        response_post_message = requests.post(f"{REQUEST_URL}/message", json={"text": self.text})
        self.assertEqual(response_post_message.status_code, 200, response_post_message.reason)

        secret_id = response_post_message.json()["secret_id"]
        self.assertEqual(type(secret_id), str)
        self.assertGreater(len(secret_id), 0)

        response_get_message = requests.get(f"{REQUEST_URL}/message/{secret_id}")
        self.assertEqual(response_get_message.status_code, 200)

        text = response_get_message.json()["text"]
        self.assertEqual(type(text), str)

        self.assertEqual(text, self.text)

    def test_get_not_uuid_secret(self):
        """
        get with not UUID secret -> access with wrong secret (should be denied)
        """
        wrong_secret_id = "test_wrong:("

        response = requests.get(f"{REQUEST_URL}/message/{wrong_secret_id}")
        self.assertEqual(response.status_code, 422)

    def test_get_non_existent_message(self):
        """
        get with non-existent UUID secret -> access with wrong secret (should be denied)
        """
        wrong_secret_id = uuid4()

        response = requests.get(f"{REQUEST_URL}/message/{wrong_secret_id}")
        self.assertEqual(response.status_code, 404)

    def test_double_access(self):
        """
        create message -> receive message -> receive message second time (should be denied).
        """
        response_post_message = requests.post(f"{REQUEST_URL}/message", json={"text": self.text})
        secret_id = response_post_message.json()["secret_id"]
        requests.get(f"{REQUEST_URL}/message/{secret_id}")
        response_get_message2 = requests.get(f"{REQUEST_URL}/message/{secret_id}")

        self.assertEqual(response_get_message2.status_code, 404)


if __name__ == '__main__':
    unittest.main()
