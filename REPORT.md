# Project Report

## Course Information

This project is a part of the course `[S24] System and Network Administration` at Innopolis University.

## Team

- B22-SD-02, Darya Koncheva
- B22-SD-03, Azamat Bayramov
- B22-SD-03, Vladislav Bolshakov
- B22-SD-03, Dmitriy Okoneshnikov

## Goals and Tasks of the Project

### Goals

- Developing a service that allows users to generate one-time messages by providing them with a unique link that displays the message when first opened and then shows the "not found" page.
- Service should have automated testing.
- Service should have automated deployment.
- Service should have automated database backups.
- Service should have monitoring system.
- Service should have a backend API, frontend user interface and monitoring system on the same address and port.

### Tasks

- Configure backend application interface - Dmitriy Okoneshnikov, Vladislav Bolshakov.
- Develop backend application using Python, FastAPI, MongoDB - Dmitriy Okoneshnikov.
- Containerize backend application using Docker - Dmitriy Okoneshnikov.
- Create a Docker Compose file to run the backend application and MongoDB - Dmitriy Okoneshnikov.
- Configure Nginx as a reverse proxy for API requests - Azamat Bayramov.
- Configure Nginx to limit rate of requests - Dmitriy Okoneshnikov.
- Write unit tests for the backend application - Vladislav Bolshakov.
- Set up GitLab CI/CD pipeline for backend automated testing and deployment - Azamat Bayramov.
- Develop frontend application using React - Darya Koncheva.
- Prepare frontend application for deployment - Darya Koncheva.
- Add frontend application building and deployment to GitLab CI/CD pipeline - Darya Koncheva, Azamat Bayramov.
- Configure Nginx to serve the frontend application - Azamat Bayramov, Darya Koncheva.
- Set up Prometheus and Grafana for monitoring - Azamat Bayramov.
- Configure Nginx to reverse proxy Grafana - Azamat Bayramov.
- Implement automated database backups using Cron and Bash - Vladislav Bolshakov.
- Perform security (NoSQL injections) and vulnerability (DOS, DDOS) checks - Vladislav Bolshakov.
- TTL MongoDB index for documents in the database - Vladislav Bolshakov, Dmitriy Okoneshnikov.

## Execution plan/Methodology

The project was executed using the following methodology:
- Agile development with daily stand-up meetings to track progress and identify any roadblocks.
- GitLab for version control and CI/CD to automate the build, test, and deployment process.
- Docker for containerization to package the application and its dependencies into a single unit for easy deployment.
- Nginx for reverse proxying and rate limiting to handle incoming requests and protect the application from
malicious traffic.
- Prometheus and Grafana for monitoring to collect and visualize metrics about the application's performance and health.
- Development of Solution/Tests as the PoC

### Scheme of solution

<img src="/assets/schema.jpg" alt="Application schema" width="1000">

## Development of solution/Tests as the PoC

### Backend application

The backend application was developed using Python, FastAPI, and MongoDB.
It provides the core functionality of the application, such as handling user requests,
processing data, and interacting with the database. Unit tests were written to ensure the correctness
of the backend application's functionality.

The backend structure is designed to be flexible and easy to maintain.
The use of separate files for different parts of the application makes it easy to add new features and functionality.
The use of a dependency injection framework like FastAPI makes it easy to test the application and to ensure that the
dependencies are correctly configured.


### Frontend Application

The frontend application was developed using React. It provides the responsive user interface and interacts
with the backend application through API requests. The frontend application was prepared for deployment
by building a production-ready bundle. Vite was employed as the build tool for assembling the application.

### Infrastructure

Nginx was configured as a reverse proxy to handle incoming requests and route them to the appropriate backend service.
It was also configured to limit the rate of requests to protect the application from denial-of-service attacks.
Prometheus and Grafana were set up to collect and visualize metrics about the application's performance and health. 
Nginx was also configured to reverse proxy Grafana, making it accessible through a web interface.

### CI/CD Pipeline

A GitLab CI/CD pipeline was set up to automate the build, test, and deployment process for both the backend and frontend applications.
The pipeline includes stages for building the Docker images, running unit tests, deploying the application to a test
environment, and finally deploying the application to production.

### Database Backups

Automated database backups were implemented using Cron and Bash. A Bash script was created to dump the database to a file,
and Cron was used to schedule the script to run on a regular basis.

## Difficulties faced, new skills acquired during the project

### Difficulties

The team encountered the following difficulties during the project:

- Integrating the frontend and backend applications: This required careful planning and coordination
between the frontend and backend teams to ensure that the two applications communicated seamlessly.
- Configuring Nginx for rate limiting: The team had to research and experiment with different Nginx
configuration options to find the optimal settings for rate limiting.
- Setting up Prometheus and Grafana: The team had to learn how to configure Prometheus to collect
the necessary metrics and how to use Grafana to visualize the data.

### New skills

The team acquired the following new skills during the project:

- Python, FastAPI, MongoDB: The backend team gained proficiency
in these technologies for developing and testing the backend application.
- Docker, Docker Compose: The team learned how to use Docker for containerization
and how to manage multiple containers using Docker Compose.
- Nginx configuration: The team gained a deep understanding of Nginx configuration options
for reverse proxying and rate limiting.
- Prometheus, Grafana: The team learned how to use Prometheus for monitoring and Grafana for data visualization.
- GitLab CI/CD: The team gained experience in setting up and using GitLab CI/CD for automated testing and deployment.
Conclusion
- React, Vite, npm: The team seamlessly integrated the frontend into our CI/CD pipeline and Nginx,
connecting it with all components of our application.


## Conclusion, contemplations and judgment

### Judgements

The project experience has led the team to the following contemplations and judgments:
- The importance of using a structured methodology and tools for software development:
Using Agile development, GitLab CI/CD, and Docker helped the team to work efficiently
and deliver a high-quality product.
- The benefits of containerization and CI/CD for efficient deployment: Containerization
and CI/CD streamlined the deployment process and made it possible to deploy the application quickly and reliably.
- The need for monitoring and alerting to ensure application reliability: Monitoring and
alerting are essential for ensuring that the application is performing as expected and for
identifying and resolving issues quickly.

### Possible improvements of our system and some limitations

- Given that everyone currently has unrestricted access to our documentation and metrics, 
we can improve security of our system. One effective measure is to
restrict access to these sensitive resources. By carefully controlling who can view it,
we can mitigate the risk of unauthorized access and potential data breaches.
- At present, there are no limitations on the size of the request body to the backend,
which could potentially lead to database overload due to a high volume of requests.
We can improve system by adding limit on the size of the request's body.
- We could also move from HTTP to HTTPS, so attackers could not just view the requests and responses using, e.g., Wireshark and intercept the secret IDs.

### Links

Link to our service: <http://otms.duckdns.org/>

Documentation: <http://otms.duckdns.org/api/docs>

Dashboards (username: `test1` and password: `test1`): <http://otms.duckdns.org/grafana>

Repository on the GitLab: <https://gitlab.com/azamatbayramov/otms>

Video demonstration (demo): <https://www.youtube.com/watch?v=fSCHhEFliUc>
